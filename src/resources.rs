use serde::{Serialize, Deserialize};


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Port {
    #[serde(rename="Label")]
    pub label: String,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NetworkResource {
    #[serde(rename="Mode", skip_serializing_if = "Option::is_none")]
    pub mode: Option<String>,

    #[serde(rename="DynamicPorts")]
    pub port: Vec<Port>,
}
