use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ServiceCheck {
    #[serde(rename="Name")]
    pub name: Option<String>,

    #[serde(rename="Type")]
    pub _type: String,

    #[serde(rename="Path")]
    pub path: Option<String>,

    #[serde(rename="Interval", skip_serializing_if = "Option::is_none")]
    pub interval: Option<i64>,

    #[serde(rename="Timeout", skip_serializing_if = "Option::is_none")]
    pub timeout: Option<i64>,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Service {
    #[serde(rename="Name")]
    pub name: String,

    #[serde(rename="Tags")]
    pub tags: Vec<String>,

    #[serde(rename="Checks")]
    pub check: Vec<ServiceCheck>,

    #[serde(rename="PortLabel")]
    pub port: Option<String>,
}
