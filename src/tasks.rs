use serde::{Serialize, Deserialize};
use crate::services::Service;
use crate::resources::NetworkResource;
use crate::vault::Vault;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Template {
    #[serde(rename="EmbeddedTmpl")]
    pub data: String,

    #[serde(rename="DestPath")]
    pub destination: Option<String>,

    #[serde(rename="Envvars")]
    pub env: Option<bool>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TaskConfigAuth {
    pub server_address: String,
    pub username: String,
    pub password: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TaskConfig {
    pub image: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub auth: Option<TaskConfigAuth>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Task {
    #[serde(rename="Name")]
    pub name: String,

    #[serde(rename="Driver")]
    pub driver: String,

    #[serde(rename="Config")]
    pub config: TaskConfig,

    #[serde(rename="Templates")]
    pub templates: Vec<Template>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename="Vault")]
    pub vault: Option<Vault>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct TaskGroup {
    #[serde(rename="Name")]
    pub name: String,

    #[serde(rename="Count", skip_serializing_if = "Option::is_none")]
    pub count: Option<u32>,

    #[serde(rename="Tasks")]
    pub tasks: Vec<Task>,

    #[serde(rename="Networks")]
    pub network: Vec<NetworkResource>,

    #[serde(rename="Services")]
    pub services: Vec<Service>,
}


// #[derive(Debug, Clone, Serialize, Deserialize)]
// pub struct Group {
//     #[serde(rename="Name")]
//     pub name: String,

//     #[serde(rename="Tasks")]
//     pub tasks: Vec<Task>
// }
