use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Constraint {
    #[serde(rename="LTarget", skip_serializing_if = "Option::is_none")]
    pub attribute: Option<String>,

    #[serde(rename="RTarget", skip_serializing_if = "Option::is_none")]
    pub value: Option<String>,

    #[serde(rename="Operand", skip_serializing_if = "Option::is_none")]
    pub operator: Option<String>,
}
