use serde::{Serialize, Deserialize};
use std::error::Error;
use std::fmt;
use reqwest::StatusCode;
use crate::constraint::Constraint;
use crate::tasks::TaskGroup;
// pub fn ParseHCL(jobHCL string, canonicalize bool) (*Job, error) {
// 	var job Job
// 	req := &JobsParseRequest{
// 		JobHCL:       jobHCL,
// 		Canonicalize: canonicalize,
// 	}
// 	_, err := j.client.write("/v1/jobs/parse", req, &job, nil)
// 	return &job, err
// }

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Job {
    #[serde(rename="ID")]
    pub id: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename="Region")]
    pub region: Option<String>,

    #[serde(rename="Datacenters")]
    pub datacenters: Vec<String>,

    #[serde(rename="Constraints")]
    pub constraint: Vec<Constraint>,

    #[serde(rename="TaskGroups")]
    pub task_groups: Vec<TaskGroup>,
    // #[serde(rename="Canonicalize")]
    // canonicalize: bool,
}

impl fmt::Display for Job {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string(&self).unwrap())
    }
}

#[derive(Serialize)]
struct JobRegisterRequest<'a> {
    #[serde(rename="Job")]
    job: &'a Job,
}

#[derive(Serialize, Deserialize)]
struct JobsParseRequest {
    #[serde(rename="JobHCL")]
    job_hcl: String,

    #[serde(rename="Canonicalize")]
    canonicalize: bool,
}

/// ParseHCL is used to convert the HCL repesentation of a Job to JSON server side.
///
/// To parse the HCL client side see package github.com/hashicorp/nomad/jobspec
pub async fn parse_hcl<S: Into<String> + std::fmt::Display>(
    job_hcl: S,
    canonicalize: bool
) -> Result<Job, Box<dyn Error>> {
    let client = reqwest::Client::new();
    let url = "http://nomad.service.consul:4646/v1/jobs/parse";

    // Parse Nomad job into JSON
    match client.post(url).body(match serde_json::to_string(&JobsParseRequest {
        job_hcl: job_hcl.to_string(),
        canonicalize: canonicalize,
    }) {
        Ok(json) => json,
        Err(err) => return Err(Box::new(err))
    }).send().await {
        Ok(res) => match res.status() {
            StatusCode::OK => Ok(match res.text().await {
                Ok(txt) => {
                    let job: Job = serde_json::from_str(&txt).unwrap();
                    job
                },
                Err(error) => return Err(error.into())
            }),
            _ => Err("Not 200 response from Nomad".into()),
        },
        Err(error) => Err(error.into()),
    }
}


/// Reads information about a single job for its specification and status
pub async fn job_read(job_id: String) -> Result<Job, Box<dyn Error>> {
    let client = reqwest::Client::new();
    let url = format!("http://nomad.service.consul:4646/v1/job/{}", job_id);
    match client.get(&url).send().await {
        Ok(res) => match res.status() {
            StatusCode::OK => Ok(match res.text().await {
                Ok(txt) => serde_json::from_str(&txt)?,
                Err(error) => return Err(error.into())
            }),
            _ => Err("Not 200 response from Nomad".into()),
        },
        Err(error) => Err(error.into()),
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JobRegisterResponse {
    #[serde(rename="EvalID")]
    eval_id: String,

    #[serde(rename="EvalCreateIndex")]
    eval_create_index: u64,

    #[serde(rename="JobModifyIndex")]
    job_modify_index: u64,

    #[serde(rename="Warnings")]
    warnings: String,
}

impl fmt::Display for JobRegisterResponse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string(&self).unwrap())
    }
}

#[derive(Debug)]
struct SuperError {
    msg: String,
}

impl fmt::Display for SuperError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.msg)
    }
}

impl Error for SuperError {
    // fn source(&self) -> Option<&(dyn Error + 'static)> {
    //     Some(&self.msg)
    // }
}

pub async fn register(job: &Job) -> Result<JobRegisterResponse, Box<dyn Error>> {
    let client = reqwest::Client::new();
    let url = "http://nomad.service.consul:4646/v1/jobs";
    match client.post(url).json(&JobRegisterRequest { job: job }).send().await {
        Ok(resp) => {
            if resp.status().is_success() {
                match resp.text().await {
                    Ok(txt) => {
                        match serde_json::from_str(&txt) {
                            Ok(job_register_response) => Ok(job_register_response),
                            Err(err) => Err(Box::new(err)),
                        }
                    },
                    Err(err) => Err(Box::new(err)),
                }
            } else {
                Err(Box::new(SuperError {
                    msg: resp.text().await.unwrap(),
                }))
            }
        },
        Err(err) => Err(Box::new(err)),
    }
}

// pub async fn nomad_job_parse_and_post<S: Into<String> + std::fmt::Display>(
//     job_name: S,
//     input_hcl: S,
// ) -> Result<String, Box<dyn Error>> {
//         let job = match parse_hcl(input_hcl, false).await {
//             Ok(response) => response,
//             Err(err) => return Err(err)
//         };
//         register(&job).await
// }
