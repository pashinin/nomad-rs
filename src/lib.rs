pub mod constraint;
pub mod jobs;
pub mod resources;
pub mod services;
pub mod tasks;
pub mod vault;


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
