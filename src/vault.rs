//! Vault
use serde::{Serialize, Deserialize};

#[derive(Default, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Vault {
    #[serde(rename = "Policies", skip_serializing_if = "Option::is_none")]
    pub policies: Option<Vec<String>>,
    #[serde(rename = "Env", skip_serializing_if = "Option::is_none")]
    pub env: Option<bool>,
    #[serde(rename = "ChangeMode", skip_serializing_if = "Option::is_none")]
    pub change_mode: Option<String>,
    #[serde(rename = "ChangeSignal", skip_serializing_if = "Option::is_none")]
    pub change_signal: Option<String>,
}
